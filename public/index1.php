<!DOCTYPE html>
<?php
	require_once("utilities.php");
    require_once("game.php");
 	
 	
	 $wins=0;
     $losses=0;
	$correo= $_GET['correo']; 
	
	$conn = createConnection(); 
	$query="SELECT  * FROM users WHERE Correo='".$correo."' ";
    $result = mysqli_query($conn, $query );  

    while($row = mysqli_fetch_assoc($result))
    {  
        $wins= $row['Wins']; 
        $losses= $row['Losses']; 
    }
    function addWin()
    {
		global $wins,$losses,$correo;
		
		$conn = createConnection(); 
    	$query="UPDATE users SET Wins=$wins WHERE Correo='".$correo."' ";
    	 $result = mysqli_query($conn, $query );
    }
    function addLosse()
    {
		
		global $wins,$losses,$correo;
		$conn = createConnection(); 
    	$query="UPDATE users SET losses=$losses WHERE Correo='".$correo."'";
    	 $result = mysqli_query($conn, $query );
    }
    function fetchRandomWordFromDB()
    {
		$conn = createConnection();
		$sql = "SELECT word FROM words ORDER BY RAND() LIMIT 1";
		
		$stmt = mysqli_stmt_init($conn);
		if (mysqli_stmt_prepare($stmt, $sql))
		{
		    mysqli_stmt_execute($stmt);
			mysqli_stmt_bind_result($stmt, $name);
			mysqli_stmt_fetch($stmt);
			mysqli_stmt_close($stmt);
			
			return $name;
		}
		else
			return null;
    }

	session_start();
	
	$letter='';
	
	if (!isset($_POST["letterTextBox"]) || !isset($_SESSION["game"]))
	{
		$game = new Game(fetchRandomWordFromDB());
		
		$showUserInput = true;
		
		$_SESSION["game"] = $game;
	}
	else
	{
	 	$game = $_SESSION["game"];
		$letter = $_POST["letterTextBox"];
		
		$game->play($letter);
	}
	if ($game->health==0 )
	{
		session_destroy();
	}	
	if(isset($_POST['reinicio']))
	{
  		$game = new Game(fetchRandomWordFromDB());
  		$_SESSION["game"] = $game;
  		header("Location: index1.php?correo=$correo ");

	}
	if(isset($_POST['LogOut']))
	{
  		header("Location:http://hangmanmaster.local/");

	}


?>
<html>
	<head>
        <link rel="stylesheet" type="text/css" href="css/index1.css"/>
	
	</head>
	<body>
        <header>
            <h1> Juego del Ahorcado </h1>
        </header>
         
         <section>
        <?php if ($game->health==6) { ?>
		
            <img src="images//hang_0.jpg" > </img>	
		<?php } ?>
		<?php if ( $game->health==5) { ?>
		
            <img src="images//hang_1.jpg" > </img>	
		<?php } ?>
		<?php if ($game->health==4) { ?>
		
            <img src="images//hang_2.jpg" > </img>	
		<?php } ?>
		<?php if ($game->health==3) { ?>
		
            <img src="images//hang_3.jpg" > </img>	
		<?php } ?>
		<?php if ($game->health==2) { ?>
		
            <img src="images//hang_4.jpg" > </img>	
		<?php } ?>
		<?php if ($game->health==1) { ?>
		
            <img src="images//hang_5.jpg" > </img>	
		<?php } ?>
		<?php if ($game->health==0) { ?>
		
            <img src="images//hang_6.jpg" > </img>	
		<?php } ?>
        
	    <div>
	    	<?php if(isset($_GET["new"]))
	    	{ ?><strong> Datos ingresados satisfactoriamente, Puede empezar a jugar</strong>
	    		<?php } ?>
	    	</br>
		   <strong> Vidas: <?= $game->health  ?><strong></br>
		   <strong> Partidas gandas: <?= $wins  ?><strong></br>
		   <strong> Partidas perdidas: <?= $losses  ?><strong></br>

		</div>
		
		<div class="revealed">
			
			<br/>
			<?= $game->revealed ?>
		</div>
				
		<?php if (!$game->hasFinished()) { ?>
		<div class="userInput">
			<form method="POST">
			    <input type ="text" 
				       id="letterTextBox"
             		   name="letterTextBox"
					   maxlength="1" 
					   size="1"
					   autofocus 
					   	/>
				<input type ="submit" 
				       id="tryButton"
         			   name="tryButton" 
					   value="Intentar" />
				<!--<button type="button"><a href="http://hangmanmaster.local">Reiniciar</a></button>*/-->
				<input type="submit" value="Reiniciar" name="reinicio" />
			  <input type="submit" value="LogOut" name="LogOut" />
				
			</form>		  
		</div>
		<?php } else { ?>
			<?php  if ($game->hasBeenRevealed()) {$wins=$wins+1; addWin(); ?>
			  <div>Ganaste </div>
			  <form method="POST">
			  <input type="submit" value="Reiniciar" name="reinicio" />
			  <input type="submit" value="LogOut" name="LogOut" />
			  </form>
			<?php } 
			else { $losses=$losses+1; addLosse();?>
			  <div> Perdiste </div>
			  <form method="POST">
			  <input type="submit" value="Reiniciar" name="reinicio" />
			  <input type="submit" value="LogOut" name="LogOut" />
			  </form>
			<?php } ?> 
		<?php } ?>
		</section>  
	</body>
</html>
