function validacion() 
{
	//validate name
	var valor = document.getElementById("nameTextBox").value;
	if( valor == null ||valor.length ==0|| /^\s+$/.test(valor) ) {
		alert("[ERROR] El campo de nombre no puede estar vacio");
 		 return false;	 
	} 
	if ( valor.length < 3){
		alert("[ERROR] El campo de nombre tiene que ser mayor a dos letras");
 		 return false;	
	}
	//validate lastname	
	var valor = document.getElementById("lastNameTextBox").value;
	if( valor == null ||valor.length ==0|| /^\s+$/.test(valor) ) {
		alert("[ERROR] El campo de apellido no puede estar vacio");
 		 return false;	 
	} 
	if ( valor.length < 3){
		alert("[ERROR] El campo de apellido tiene que ser mayor a dos letras");
 		 return false;	
	}
	
	//validate password
	var valor = document.getElementById("passwordTextBox").value;
	if( valor == null ||valor.length ==0|| /^\s+$/.test(valor) ) {
		alert("[ERROR] El campo de contrasena no puede estar vacio");
 		 return false;	 
	} 
	var valor1 = document.getElementById("conPasswordTextBox").value;
	if( valor1 == null ||valor1.length ==0|| /^\s+$/.test(valor1) ) {
		alert("[ERROR] El campo de validacion de contrasena no puede estar vacio");
 		 return false;	 
	} 
	//validate credit card code number 
else{
	var valor = document.getElementById("CardNumberTextBox").value;
	if ( valor.length==0 ){
		alert("[ERROR] El campo de tarjeta de credito no puede estar vacio");
 		 return false;	
	}
	}
	else
	{var valor = document.getElementById("codeCardTextBox").value;
	
	if ( (valor.length >=2 && valor.length<=4) || valor.length==1){
		alert("[ERROR] El campo de codigo de tarjeta de credito debe tener entre 2 y 4 digitos");
 		 return false;	
	}}
	else
	return false;
}